class Store < ActiveRecord::Base
  belongs_to :manager
  attr_accessible :num_employees, :street_address
end

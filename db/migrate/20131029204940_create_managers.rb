class CreateManagers < ActiveRecord::Migration
  def change
    create_table :managers do |t|
      t.string :last_name
      t.string :first_name
      t.integer :salary

      t.timestamps
    end
  end
end

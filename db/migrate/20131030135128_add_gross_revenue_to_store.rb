class AddGrossRevenueToStore < ActiveRecord::Migration
  def change
    add_column :stores, :gross_revenue, :integer
  end
end

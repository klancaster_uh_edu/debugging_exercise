class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :street_address
      t.integer :num_employees
      t.references :manager

      t.timestamps
    end
    add_index :stores, :manager_id
  end
end
